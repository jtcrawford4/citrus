/***********************
        GAME
************************/
var ELAPSED_TIME = 0;

/***********************
        LEVEL
************************/
var CURRENT_LEVEL = 1;
var CURRENT_XP = 0;
var XP_TO_NEXT_LEVEL = 100;

/***********************
        CASH
************************/
var AVAILABLE_FUNDS = 0;

/***********************
    BASIC DETAIL
************************/
var BD_CURRENT_PROGRESS = 0;
var BD_PROGRESS_PER_CLICK = 5;
var BD_PROGRESS_TO_COMPLETE_CAR = 100;
var BD_CAR_COMPLETE_BONUS_XP = 4;
var BD_CAR_COMPLETED_PAYMENT = 125;