$('#basicDetailBtn').on('click', function(){
    CURRENT_XP += 1;
    setLevelProgress() //todo go in game loop
    BD_CURRENT_PROGRESS += BD_PROGRESS_PER_CLICK
    var percentComplete = BD_CURRENT_PROGRESS > 0 ?
            Math.round((BD_CURRENT_PROGRESS / BD_PROGRESS_TO_COMPLETE_CAR) * 100) :
            0;
    if(BD_CURRENT_PROGRESS >= BD_PROGRESS_TO_COMPLETE_CAR){
        updateCarProgressBar(0);
        BD_CURRENT_PROGRESS = 0;
        CURRENT_XP += BD_CAR_COMPLETE_BONUS_XP;
        increaseAvailableFunds(BD_CAR_COMPLETED_PAYMENT);
    }else{
        updateCarProgressBar(percentComplete);
    }
});

function updateCarProgressBar(percentComplete){
    $('#basicDetailProgressBar').css('width', percentComplete + '%').attr('aria-valuenow', percentComplete);
}