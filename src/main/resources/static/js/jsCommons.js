/***********************
        CASH
************************/
function setAvailableFunds(amount){
    AVAILABLE_FUNDS = amount
    setAvailableFundsLabel();
}

function increaseAvailableFunds(amount){
    AVAILABLE_FUNDS += amount
    setAvailableFundsLabel();
}

function decreaseAvailableFunds(amount){
    AVAILABLE_FUNDS -= amount
    setAvailableFundsLabel();
}

function setAvailableFundsLabel(){
    $('#availableFunds').text(toCurrency(AVAILABLE_FUNDS));
}

function toCurrency(amount){
   return amount.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/***********************
        LEVEL
************************/
function setLevel(level){
    CURRENT_LEVEL = level;
    $('#currentLevel').text('Lvl ' + level);
}

function setLevelProgress(){
    var percentComplete = CURRENT_XP > 0 ? Math.round((CURRENT_XP / XP_TO_NEXT_LEVEL) * 100) : 0;
    $('#levelProgressBar').css('width', percentComplete + '%').attr('aria-valuenow', percentComplete);
}
