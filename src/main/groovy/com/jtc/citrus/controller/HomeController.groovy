package com.jtc.citrus.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/")
class HomeController {

    @RequestMapping("")
    static String index(){
        return "home"
    }

    @RequestMapping("upgrades")
    static String upgrades(){
        return "upgrades"
    }

    @RequestMapping("accounting")
    static String accounting(){
        return "accounting"
    }


}
