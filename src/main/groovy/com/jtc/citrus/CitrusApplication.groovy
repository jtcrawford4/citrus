package com.jtc.citrus

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class CitrusApplication {

	static void main(String[] args) {
		SpringApplication.run(CitrusApplication, args)
	}

}
